import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { ToastController } from 'ionic-angular';


@Component({
  selector: 'page-forgetpassword',
  templateUrl: 'forgetpassword.html'
})
export class ForgetpasswordPage {
  username : any;
  constructor(public navCtrl: NavController,public tstCtrl: ToastController,public menu: MenuController) {
   this.username = "";
  }
  sendPassword(){
    if(this.username){
     
    }else{
      this.doToast("Fill the username!");
    }
  }
  goToLogin(){
    this.navCtrl.setRoot(LoginPage);
  }
  doToast(msg : string){
    let tst = this.tstCtrl.create({
      message : msg,
      position : 'bottom',
      duration : 2000
    });
    tst.present();
  }
  ionViewDidEnter(){
    this.menu.enable(false);
  }
  ionViewWillLeave(){
    this.menu.enable(true);
  }
}