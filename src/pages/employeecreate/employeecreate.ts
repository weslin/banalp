import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
// import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: 'page-employeecreate',
  templateUrl: 'employeecreate.html'
})
export class EmployeeCreatePage {
    imgName: string ;
    empId: string;
    empFirstName: string;
    empLastName: string;
    empDOB: string;
    empEmail: string;
    empPhone: string; 
    empDept: string; 
    empDesi: string;
    empDOJ: String;
    empRole: string;
    gender: string;
    dateOfBirth : any;
    dateOfJoining : any;
    genderAlertOpts: { title: string, subTitle: string };
    roleAlertOpts: { title: string, subTitle: string };
    deptAlertOpts: { title: string, subTitle: string };
    desiAlertOpts: { title: string, subTitle: string };
	  
		  
  constructor(public navCtrl: NavController) {
    
	this.dateOfBirth = new Date();
    this.dateOfJoining = new Date();

    this.genderAlertOpts = {
        title: 'Gender',
        subTitle: 'Select gender'
      };
    this.roleAlertOpts = {
        title: 'Role',
        subTitle: 'Select role'
      };
    this.deptAlertOpts = {
        title: 'Department',
        subTitle: 'Select department'
      };
    this.desiAlertOpts = {
        title: 'Designation',
        subTitle: 'Select designation'
      };
	  
	  
	  

  }
}
