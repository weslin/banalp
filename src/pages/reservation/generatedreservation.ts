import { Component,ViewChild } from '@angular/core';
import { NavController,Select } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
//import {CreateClientPage} from '../createclient/createclient';
//import {NoshowPageModule} from '../noshow/noshow';

@Component({
  selector: 'page-generatedreservation',
  templateUrl: 'generatedreservation.html'
})
export class GeneratedReservationPage {
  @ViewChild('mySelect') selectRef: Select;
 employeeList : Array<{invoiceId:string;
 clientName:string,
 imgName:any,
 guestName:string,
 phone:string,
 hotelName:string,
 Checkinout: string,
 generatedby:string,
 amount:string}>
 
 constructor(public navCtrl: NavController,public tstCtrl: ToastController,public menu: MenuController) {
    this.employeeList=[
      {imgName:'assets/imgs/logo.png',invoiceId: '1',clientName: 'Ionic', guestName: 'Sujatha',phone: '9876543210',hotelName: 'Sweety', Checkinout:'08/16/2018 to 08/23/2018',generatedby:'Banalp',amount:'$1671'},
      {imgName:'assets/imgs/banalp-logo.png',invoiceId: '2',clientName: 'Krishna',guestName: 'Durga', phone: '8680899738' , hotelName: 'Lordly', Checkinout:'08/25/2018 to 08/28/2018',generatedby:'Banalp',amount:'$1671'},
      {imgName:'assets/imgs/logo.png',invoiceId: '3',clientName: 'Ionic',guestName:'Wesline',phone: '9876543210',hotelName:'lordly',Checkinout:'06/25/2018 to 06/28/2018',generatedby:'Banalp',amount:'$1671'},
      {imgName:'assets/imgs/banalp-logo.png',invoiceId: '4',clientName: 'Ionic',guestName:'Bala',phone: '9876512345',hotelName:'lordly',Checkinout:'06/25/2018 to 06/28/2018',generatedby:'Banalp',amount:'$1671'}
    ];

  }	
 
}
