import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { EnquiryDetailPage } from '../enquiryviewdetail/enquiryviewdetail';

@Component({
  selector: 'page-createenquiry',
  templateUrl: 'createenquiry.html'
})
export class CreateEnquiryPage {
	
	companyName=[{name:"appsinbox"},{name:"hello"},{name:"banalp"}];
  client: string;
  clientAlertOpts: { title: string, subTitle: string };
  enqList : Array<{imgName: string,enqId:string,enqCreateDate: string,guestName: string, noofOccupency: string, noofRooms: string, visitAddress: string, checkinout: string, generateby: string}>;
  constructor(public navCtrl: NavController,public tstCtrl: ToastController,public menu: MenuController) {
    this.enqList=[
      {imgName:'assets/imgs/banalp-logo.png',enqId: '01',enqCreateDate: '03-08-2018',guestName: 'Chiranjeevi',noofOccupency: '2',noofRooms: '2',visitAddress: '12-25/52, Kattupakkam,chennai-600056',checkinout:'03-08-2018 to 05-08-2018',generateby:'Krishna'},
    ];
    this.clientAlertOpts = {
      title: 'Clients',
      subTitle: 'Select client below!'
    };
  }
  openDetail(list){
    //alert("Clicked me");
    this.navCtrl.push(EnquiryDetailPage, {
      imgName: list.imgName,
      enqId:list.enqId,
      enqCreateDate: list.enqCreateDate,
      guestName: list.guestName,
      noofOccupency: list.noofOccupency, 
      noofRooms: list.noofRooms, 
      visitAddress: list.visitAddress, 
      checkinout: list.checkinout, 
      generateby: list.generateby});
  }
}