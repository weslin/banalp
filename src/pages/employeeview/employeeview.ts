import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

import { EmployeeDetailPage } from '../employeeviewdetail/employeeviewdetail';
import { EmployeeCreatePage} from '../employeecreate/employeecreate';

@Component({
  selector: 'page-employeeview',
  templateUrl: 'employeeview.html'
})
export class EmployeeViewPage {
  status:string ="Active";
  toggle01: boolean = true;
toggle02: boolean = false;
 employeeList : Array<{imgName: string,empId: string,empName: string, empDOB: string, empEmail: string, empPhone: string, empDept: string, empDesi: string, empDOJ: String}>;

 constructor(public navCtrl: NavController,public tstCtrl: ToastController,public menu: MenuController) {
  
    this.employeeList=[
      {imgName:'assets/imgs/logo.png',empId: '1',empName: 'Ionic',empDOB:'18-01-1991',empEmail: 'a.krrish777@gmail.com',empPhone: '9876543210',empDept: 'IONIC',empDesi: 'ionic Developer',empDOJ: '05-10-2015'},
      {imgName:'assets/imgs/banalp-logo.png',empId: '2',empName: 'Krishna',empDOB:'18-01-1991',empEmail: 'a.krrish777@gmail.com',empPhone: '8680899738',empDept: 'Mobile',empDesi: 'iOS Developer',empDOJ: '05-10-2015'},
      {imgName:'assets/imgs/logo.png',empId: '1',empName: 'Ionic',empDOB:'18-01-1991',empEmail: 'a.krrish777@gmail.com',empPhone: '9876543210',empDept: 'IONIC',empDesi: 'ionic Developer',empDOJ: '05-10-2015'},
      
      {imgName:'assets/imgs/banalp-logo.png',empId: '2',empName: 'Krishna',empDOB:'18-01-1991',empEmail: 'a.krrish777@gmail.com',empPhone: '8680899738',empDept: 'Mobile',empDesi: 'iOS Developer',empDOJ: '05-10-2015'}
    ];
  }
  openDetail(list,index){
   // alert("Clicked me");
   console.log("Clicked on:",list);
   this.navCtrl.push(EmployeeDetailPage, { imgName: list.imgName, empName: list.empName, empId: list.empId, empDOB: list.empDOB, empPhone: list.empPhone, empEmail: list.empEmail, empDept: list.empDept, empDesi: list.empDesi, empDOJ: list.empDOJ});
   //console.log(this.employeeList[index].empName);
    
  } 
  openViewdetail(){
    this.navCtrl.push( EmployeeCreatePage);
  }
 }     

