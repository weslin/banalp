import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

import { CoordinatorDetailPage } from '../coordinatorviewdetail/coordinatorviewdetail';

@Component({
  selector: 'page-createcoordinator',
  templateUrl: 'createcoordinator.html'
})
export class CreateCoordinatorPage {
	gender:string;
  dateofbirth:any;
  genderAlertOpts:any;
	companyName=[{name:"appsinbox"},{name:"hello"},{name:"banalp"}];
	
  coordList : Array<{imgName: string,coordId:string,coordName: string,type: string, gender: string, DOB: string, phone: string, mail: string, passport: string}>;
  constructor(public navCtrl: NavController,public tstCtrl: ToastController,public menu: MenuController) {
    this.coordList=[
      
      {imgName:'assets/imgs/banalp-logo.png',coordId: '01',coordName: 'Pawan Kalyan',type: 'Travel Coordinator',gender: 'Male',DOB: '18-01-2018',phone: '8680899738',mail:'a.krrish777@gnail.com',passport:'123456'},
    ];
	
	this.genderAlertOpts = {
        title:'Gender',
        subTitle: 'Select gender'
      };
	  
  }
  openDetail(list){
    //alert("Clicked me");
    this.navCtrl.push(CoordinatorDetailPage,{
      imgName: list.imgName,
      coordId:list.coordId,
      coordName: list.coordName,
      type: list.type,
      gender: list.gender,
      DOB: list.DOB, 
      phone: list.phone, 
      mail: list.mail, 
      passport: list.passport
    });
  }
}
  