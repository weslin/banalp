import { Component } from '@angular/core';
import { NavController,NavParams} from 'ionic-angular';

@Component({
  selector: 'page-misreportdetail',
  templateUrl: 'misreportdetail.html'
})
export class MISReportDetailPage {
  imgName: string;
  clientName: string;
  guestName: string;
  checkinout: string;
  noofnights: string;
  confirmationNumber: string;
  savingsUSDCurrency: string;
  status: string;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.imgName = navParams.get('imgName');
    this.clientName = navParams.get('clientName');
    this.guestName = navParams.get('guestName');
    this.checkinout = navParams.get('checkinout');
    this.noofnights = navParams.get('noofnights');
    this.confirmationNumber = navParams.get('confirmationNumber');
    this.savingsUSDCurrency = navParams.get('savingsUSDCurrency');
    this.status = navParams.get('status');

    console.log('Emp Detail page:' + this.clientName);
  }
 
}
