import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { PaymentHistoryDetailPage } from '../paymenthistorydetail/paymenthistorydetail';
@Component({
  selector: 'page-paymenthistory',
  templateUrl: 'paymenthistory.html'
})
export class PaymentHistoryPage {
  client: string;
  musicAlertOpts: { title: string, subTitle: string };
  clientList : Array<{value: string, ItemName: string}>;
  paymentHistoryList : Array<{imgName: string,invoiceId:string,clientName: string,guestName: string, propertyName: string, amount: string, paidAmount: string, pendingAmount: string}>;
  constructor(public navCtrl: NavController,public tstCtrl: ToastController,public menu: MenuController) {
    this.paymentHistoryList=[
      {imgName:'assets/imgs/banalp-logo.png',invoiceId: '01',clientName: 'KTF',guestName: 'Chiranjeevi',propertyName: 'Novatel',amount: '2000.00',paidAmount: '1000.00',pendingAmount:'1000.00'},
    ];
    this.clientList=[
     {value: 'ktf', ItemName: 'KTF'},
     {value: 'tcs', ItemName: 'TCS'},
     {value: 'cng', ItemName: 'Congnizent'},
    ];
    this.musicAlertOpts = {
      title: 'Clients',
      subTitle: 'Select client below!'
    };
  }
  openDetail(list){
    this.navCtrl.push(PaymentHistoryDetailPage,  {  
    imgName: list.imgName,
    invoiceId: list.invoiceId,
    clientName: list.clientName,
    guestName: list.guestName,
    propertyName: list.propertyName,
    amount: list.amount,
    paidAmount: list.paidAmount,
    pendingAmount: list.pendingAmount});
  }
  stpSelect(){

  }

}
