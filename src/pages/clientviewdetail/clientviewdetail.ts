import { Component } from '@angular/core';
import { NavController,NavParams} from 'ionic-angular';

@Component({
  selector: 'page-clientviewdetail',
  templateUrl: 'clientviewdetail.html'
})
export class ClientDetailPage {
  imgName: string;
  clientName: string;
  clientEmail: string;
  clientPhone: string;
  clientAddress: string;
  reportEmail: string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.imgName = navParams.get('imgName');
    this.clientName = navParams.get('clientName');
    this.clientEmail = navParams.get('clientEmail');
    this.clientPhone = navParams.get('clientPhone');
    this.clientAddress = navParams.get('clientAddress');
    this.reportEmail = navParams.get('reportEmail');
  
    console.log('Emp Detail page:' + this.clientName);
  }
 
}
