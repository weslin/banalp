import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { ClientWiseReportDetailPage } from '../clientwisereportdetail/clientwisereportdetail';
@Component({
  selector: 'page-clientwisereport',
  templateUrl: 'clientwisereport.html'
})
export class ClientWiseReportPage {
    client: string;
    clientAlertOpts: { title: string, subTitle: string };
  clientwiseList : Array<{imgName: string,guestName: string, checkin: string, checkout: string, noofnights: string, confirmationNumber: string, savingsUSDCurrency: string, status: string}>;
  constructor(public navCtrl: NavController,public tstCtrl: ToastController,public menu: MenuController) {
    this.clientwiseList=[
      {imgName:'assets/imgs/banalp-logo.png',guestName: 'Pawan Kalyan',checkin: '07-08-2018' , checkout: '10-08-2018',noofnights: '2',confirmationNumber: '2',savingsUSDCurrency: 'USD100',status:'Active'},
    ];
    this.clientAlertOpts = {
        title: 'Clients',
        subTitle: 'Select client below!'
      };
  }

  openDetail(list){
    this.navCtrl.push(ClientWiseReportDetailPage, {
     imgName: list.imgName,
     guestName: list.guestName,
     checkinout: list.checkinout,
     noofnights: list.noofnights,
     confirmationNumber: list.confirmationNumber,
     savingsUSDCurrency: list.savingsUSDCurrency,
     status: list.status
    });
   }
}
