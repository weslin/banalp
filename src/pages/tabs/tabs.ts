import { Component } from '@angular/core';

import { ChangepasswordPage } from '../changepassword/changepassword';
import { HomePage } from '../home/home';
import { NavParams } from 'ionic-angular';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
//mySelectedIndex: number;

  tab1Root = HomePage;
  tab2Root = ChangepasswordPage;

  constructor(public navParams: NavParams) {
   // this.mySelectedIndex = navParams.data.tabIndex || 0;
   // console.log('Selected Index',this.mySelectedIndex);
  }


}
