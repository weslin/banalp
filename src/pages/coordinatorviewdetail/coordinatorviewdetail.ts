import { Component } from '@angular/core';
import { NavController,NavParams} from 'ionic-angular';

@Component({
  selector: 'page-coordinatorviewdetail',
  templateUrl: 'coordinatorviewdetail.html'
})
export class CoordinatorDetailPage {
      imgName: string;
      coordId:string;
      coordName:string;
      type: string;
      gender: string;
      DOB: string; 
      phone: string; 
      mail: string; 
      passport: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

this.imgName = navParams.get('imgName');
    this.coordId = navParams.get('coordId');
    this.coordName = navParams.get('coordName');
    this.type = navParams.get('type');
    this.gender = navParams.get('gender');
    this.DOB = navParams.get('DOB');
    this.phone = navParams.get('phone');
    this.mail = navParams.get('mail');
    this.passport = navParams.get('passport');
  }

}
