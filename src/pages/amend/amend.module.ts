import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AmendPage } from './amend';

@NgModule({
  declarations: [
    AmendPage,
  ],
  imports: [
    IonicPageModule.forChild(AmendPage),
  ],
})
export class AmendPageModule {}
