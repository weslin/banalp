import { Component } from '@angular/core';
import { NavController,NavParams} from 'ionic-angular';

@Component({
  selector: 'page-reservationdetail',
  templateUrl: 'reservationdetail.html'
})
export class ReservationDetailPage {
  imgName: string;
  clientName: string;
  guestName: string;
  phone: string;
  hotelName : string;
  checkinout: string;
  generatedby: string;
  amountReser: string; 
  status: string
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.imgName = navParams.get('imgName');
    this.clientName = navParams.get('clientName');
    this.guestName = navParams.get('guestName');
    this.phone = navParams.get('phone');
    this.hotelName = navParams.get('hotelName');
    this.checkinout = navParams.get('checkinout');
    this.generatedby = navParams.get('generatedby');
    this.amountReser = navParams.get('amountReser');
    this.status = navParams.get('status');    
    console.log('Emp Detail page:' + this.clientName);
  }
 
}
