import { Component } from '@angular/core';
import { NavController,NavParams} from 'ionic-angular';

@Component({
  selector: 'page-enquiryviewdetail',
  templateUrl: 'enquiryviewdetail.html'
})
export class EnquiryDetailPage {
  imgName: string;
  enqId:string;
  enqCreateDate: string;
  guestName: string;
  noofOccupency: string; 
  noofRooms: string; 
  visitAddress: string; 
  checkinout: string; 
  generateby: string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.imgName = navParams.get('imgName');
    this.enqId = navParams.get('enqId');
    this.enqCreateDate = navParams.get('enqCreateDate');
    this.guestName = navParams.get('guestName');
    this.noofOccupency = navParams.get('noofOccupency');
    this.noofRooms = navParams.get('noofRooms');
    this.visitAddress = navParams.get('visitAddress');
    this.checkinout = navParams.get('checkinout');
    this.generateby = navParams.get('generateby');
  }
  generateResevation(){
    
  }

}
