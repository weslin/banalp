import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AmendReservPage } from './amend-reserv';

@NgModule({
  declarations: [
    AmendReservPage,
  ],
  imports: [
    IonicPageModule.forChild(AmendReservPage),
  ],
})
export class AmendReservPageModule {}
