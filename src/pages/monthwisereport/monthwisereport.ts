import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { MonthWiseReportDetailPage } from '../monthwisereportdetail/monthwisereportdetail';

@Component({
  selector: 'page-monthwisereport',
  templateUrl: 'monthwisereport.html'
})
export class MonthWiseReportPage {
    year: string;
    month: string;
  musicAlertOpts: { title: string, subTitle: string };
  musicAlertOpts1: { title: string, subTitle: string };
  monthwiseList : Array<{imgName: string,guestName: string, checkin: string, checkout: string, noofnights: string, confirmationNumber: string, savingsUSDCurrency: string, status: string}>;
  constructor(public navCtrl: NavController,public tstCtrl: ToastController,public menu: MenuController) {
    this.monthwiseList=[
      {imgName:'assets/imgs/banalp-logo.png',guestName: 'Pawan Kalyan',checkin: '07-08-2018' , checkout: '10-08-2018',noofnights: '2',confirmationNumber: '2',savingsUSDCurrency: 'USD100',status:'Active'},
    ];
    this.musicAlertOpts = {
        title: 'Year',
        subTitle: 'Select Year'
    };
    this.musicAlertOpts1 = {
        title: 'Month',
        subTitle: 'Select Month'
    };
  }
  openDetail(list){
    this.navCtrl.push(MonthWiseReportDetailPage, {
     imgName: list.imgName,
     guestName: list.guestName,
     checkinout: list.checkinout,
     noofnights: list.noofnights,
     confirmationNumber: list.confirmationNumber,
     savingsUSDCurrency: list.savingsUSDCurrency,
     status: list.status
    });
   }
  onSearch(){
      
  }
}
