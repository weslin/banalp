import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

@Component({
  selector: 'page-changepassword',
  templateUrl: 'changepassword.html'
})
export class ChangepasswordPage {
  old_password:any;
  new_password:any;
  confirm_password:any;
  constructor(public navCtrl: NavController, public tstCtrl: ToastController) {
    this.old_password = "";
    this.new_password = "";
    this.confirm_password = "";
  }
  updatePassword(){
    if(this.old_password!='' && this.new_password!='' && this.confirm_password!='') {
       if(this.new_password === this.confirm_password){
         //update password..
       }else{
       this.doToast("New password & Confirm passsword do not match!");
       }
    }else{
      this.doToast("Fill all the fields!");
    }
  }
  doToast(msg : string){
    let tst = this.tstCtrl.create({
      message : msg,
      position : 'bottom',
      duration : 2000
    });
    tst.present();
  }
}