import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

import { ClientDetailPage } from '../clientviewdetail/clientviewdetail';

@Component({
  selector: 'page-createclient',
  templateUrl: 'createclient.html'
})
export class CreateClientPage {
	gender:string;
	dateofbirth:any;
	clientList : Array<{imgName: string,clientName: string,clientEmail: string, clientPhone: string, clientAddress: string, reportEmail: string}>;
  constructor(public navCtrl: NavController,public tstCtrl: ToastController,public menu: MenuController) {
    this.clientList=[
      
      {imgName:'assets/imgs/banalp-logo.png',clientName: 'KTF',clientEmail: 'ktf@gmail.com',clientPhone: '8680899738',clientAddress: '2-3564/54,Kattupakkam,Chennai-600056',reportEmail: 'a.krrish777@gmail.com'},
    ];
	
	  
	  
  }
  openDetail(list){
  //  alert("Clicked me");
  this.navCtrl.push(ClientDetailPage,  {  
    imgName: list.imgName,
    clientName: list.clientName,
    clientEmail: list.clientEmail,
    clientPhone: list.clientPhone,
    clientAddress: list.clientAddress,
    reportEmail: list.reportEmail});
  }
   
}
  