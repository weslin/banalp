import { Component } from '@angular/core';
import { NavController,NavParams} from 'ionic-angular';

@Component({
  selector: 'page-employeeviewdetail',
  templateUrl: 'employeeviewdetail.html'
})
export class EmployeeDetailPage {
  imgName: string;
  empId: string;
  empName: string;
  empDOB: string;
  empEmail: string;
  empPhone: string;
  empDept: string;
  empDesi: string;
  empDOJ: string;
  
  index: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
   this.imgName = navParams.get('imgName');
   this.empId = navParams.get('empId');
   this.empName = navParams.get('empName');
   this.empDOB = navParams.get('empDOB');
   this.empEmail = navParams.get('empEmail');
   this.empPhone = navParams.get('empPhone');
   this.empDept = navParams.get('empDept');
   this.empDesi = navParams.get('empDesi');
   this.empDOJ = navParams.get('empDOJ');
   
   console.log('Emp Detail page:' + this.empName);
  }
}

