import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';


import { ForgetpasswordPage } from '../forgetpassword/forgetpassword';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  username : any;
  password : any;
  constructor(public navCtrl: NavController, public menu: MenuController,public tstCtrl: ToastController) {
    this.username = "";
    this.password = ""; 
 
  }

  checkLogin(){
    if(this.username!='' && this.password!='') {
    this.navCtrl.setRoot(TabsPage);
    }else{
      this.doToast("Fill both the fields!");
    }
  }
  forgetPass(){
    this.navCtrl.setRoot(ForgetpasswordPage);
  }
  doToast(msg : string){
    let tst = this.tstCtrl.create({
      message : msg,
      position : 'bottom',
      duration : 2000
    });
    tst.present();
  }
  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }
  ionViewWillLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }
}
