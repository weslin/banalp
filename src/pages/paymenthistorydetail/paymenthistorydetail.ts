import { Component } from '@angular/core';
import { NavController,NavParams} from 'ionic-angular';

@Component({
  selector: 'page-paymenthistorydetail',
  templateUrl: 'paymenthistorydetail.html'
})
export class PaymentHistoryDetailPage {

  imgName: string;
  invoiceId: string;
  clientName: string;
  guestName: string;
  propertyName: string;
  amount: string;
  paidAmount: string;
  pendingAmount: string;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.imgName = navParams.get('imgName');
    this.invoiceId = navParams.get('invoiceId');
    this.clientName = navParams.get('clientName');
    this.guestName = navParams.get('guestName');
    this.propertyName = navParams.get('propertyName');
    this.amount = navParams.get('amount');
    this.paidAmount = navParams.get('paidAmount');
    this.pendingAmount = navParams.get('pendingAmount');
  
    console.log('Emp Detail page:' + this.clientName);
  }
 
}
