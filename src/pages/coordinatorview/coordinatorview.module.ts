import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CoordinatorviewPage } from './coordinatorview';

@NgModule({
  declarations: [
    CoordinatorviewPage,
  ],
  imports: [
    IonicPageModule.forChild(CoordinatorviewPage),
  ],
})
export class CoordinatorviewPageModule {}
