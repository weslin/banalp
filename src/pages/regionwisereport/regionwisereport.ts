import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { RegionWiseReportDetailPage } from '../regionwisereportdetail/regionwisereportdetail';
@Component({
  selector: 'page-regionwisereport',
  templateUrl: 'regionwisereport.html'
})
export class RegionWiseReportPage {
  country: string;
  regionList : Array<{imgName: string,clientName:string,guestName: string, checkinout: string, noofnights: string, confirmationNumber: string, savingsUSDCurrency: string, status: string}>;
  constructor(public navCtrl: NavController,public tstCtrl: ToastController,public menu: MenuController) {
    this.regionList=[
      {imgName:'assets/imgs/banalp-logo.png',clientName: 'KTF',guestName: 'Pawan Kalyan',checkinout: '07-08-2018 to 10-08-',noofnights: '2',confirmationNumber: '2',savingsUSDCurrency: 'USD100',status:'Active'},
    ];
  }
  openDetail(list){
    this.navCtrl.push(RegionWiseReportDetailPage, {
     imgName: list.imgName,
     clientName: list.clientName,
     guestName: list.guestName,
     checkinout: list.checkinout,
     noofnights: list.noofnights,
     confirmationNumber: list.confirmationNumber,
     savingsUSDCurrency: list.savingsUSDCurrency,
     status: list.status
    });
 
   }
  onSearch(){
      alert('clicked!');
  }
}
