import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientviewPage } from './clientview';

@NgModule({
  declarations: [
    ClientviewPage,
  ],
  imports: [
    IonicPageModule.forChild(ClientviewPage),
  ],
})
export class ClientviewPageModule {}
