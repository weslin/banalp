import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
@Component({
  selector: 'page-employeeattendance',
  templateUrl: 'employeeattendance.html'
})
export class EmployeeAttendancePage {
    employee: string;
    musicAlertOpts: { title: string, subTitle: string };
  employeeattendanceList : Array<{imgName: string,empName: string, phone: string, loggedin: string, loggedout: string, dept: string, desi: string}>;
  constructor(public navCtrl: NavController,public tstCtrl: ToastController,public menu: MenuController) {
    this.employeeattendanceList=[
      {imgName:'assets/imgs/banalp-logo.png',phone: '9876543210',empName: 'Pawan Kalyan',loggedin: '08:30 AM' , loggedout: '05:30 PM',dept: 'Web-Develpment',desi: 'Java Developer'},
    ];
    this.musicAlertOpts = {
        title: 'Employee Name',
        subTitle: 'Select employee below!'
      };
  }
}
