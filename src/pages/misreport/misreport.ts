import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { MISReportDetailPage } from '../misreportdetail/misreportdetail';
@Component({
  selector: 'page-misreport',
  templateUrl: 'misreport.html'
})
export class MISReportPage {
  misList : Array<{imgName: string,clientName:string,guestName: string, checkinout: string, noofnights: string, confirmationNumber: string, savingsUSDCurrency: string, status: string}>;
  constructor(public navCtrl: NavController,public tstCtrl: ToastController,public menu: MenuController) {
    this.misList=[
      {imgName:'assets/imgs/banalp-logo.png',clientName: 'KTF',guestName: 'Pawan Kalyan',checkinout: '07-08-2018 to 10-08-',noofnights: '2',confirmationNumber: '2',savingsUSDCurrency: 'USD100',status:'Active'},
    ];
  }
  openDetail(list){
   this.navCtrl.push(MISReportDetailPage, {
    imgName: list.imgName,
    clientName: list.clientName,
    guestName: list.guestName,
    checkinout: list.checkinout,
    noofnights: list.noofnights,
    confirmationNumber: list.confirmationNumber,
    savingsUSDCurrency: list.savingsUSDCurrency,
    status: list.status
   });

  }
}
