import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';

import { EmployeeViewPage } from '../pages/employeeview/employeeview';
import { CreateClientPage } from '../pages/createclient/createclient';
import { CreateCoordinatorPage } from '../pages/createcoordinator/createcoordinator';
import { CreateEnquiryPage } from '../pages/createenquiry/createenquiry';
import { GeneratedReservationPage } from '../pages/reservation/generatedreservation';
import { PaymentHistoryPage } from '../pages/paymenthistory/paymenthistory';
import { MISReportPage } from '../pages/misreport/misreport';
import { RegionWiseReportPage } from '../pages//regionwisereport/regionwisereport';
import { EmployeeAttendancePage } from '../pages/employeeattendance/employeeattendance';
import { ClientCredentialsPage } from '../pages/clientcredentials/clientcredentials';
import { CoordinatorCredentialsPage } from '../pages/coordinatorcredentials/coordinatorcredentials';
import { EmployeeCredentialsPage } from '../pages/employeecredentials/employeecredentials';
import { EmailSettingsPage } from '../pages/emailsettings/emailsettings';
import { ClientWiseReportPage } from '../pages/clientwisereport/clientwisereport';
import { MonthWiseReportPage } from '../pages/monthwisereport/monthwisereport';

import { EmployeeCreatePage } from '../pages/employeecreate/employeecreate';
// import { NoshowPageModule } from '../pages/noshow/noshow.module';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = LoginPage;
  index: any;
  loginType: string;
  /* BA - Banalp Admin 
     BE - Banalp Employee
     CA - Client Admin
     CE - Client Employee
  */
  BApages: Array<{title: string, component: any}>;
  BEpages: Array<{title: string, component: any}>;
  CApages: Array<{title: string, component: any}>;
  CEpages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();
    this.loginType = "Banalp Admin";
    //used for an example of ngFor and navigation
    //BAnalp Admin Menu
    this.BApages = [
      { title: 'Home', component: HomePage },
      {title: 'Employee panel', component: EmployeeViewPage},
      {title: 'Create Employee', component: EmployeeCreatePage },
      {title: 'View Employee', component: EmployeeViewPage},
      {title: 'Client Info', component: CreateClientPage},
      {title: 'Create Client', component: CreateClientPage },
      {title: 'View Client', component: CreateClientPage},
      {title: 'Coordinator Info', component: CreateCoordinatorPage},
      {title: 'Create Coordinator', component: CreateCoordinatorPage },
      {title: 'View Coordinator', component: CreateCoordinatorPage},
      {title: 'Enquiry panel', component: CreateEnquiryPage},
      {title: 'Create Enquiry', component: CreateEnquiryPage },
      {title: 'View Enquiry', component: CreateEnquiryPage},
      {title: 'Reservation', component: GeneratedReservationPage},
      {title: 'View Generated Reservation', component: GeneratedReservationPage},
      {title: 'Payment', component: PaymentHistoryPage },
      {title: 'Make Payment', component: PaymentHistoryPage},
      {title: 'Payment History', component: PaymentHistoryPage },
      {title: 'Reports', component: MISReportPage},
      {title: 'MIS Report', component: MISReportPage},
      {title: 'Client Wise Report', component: ClientWiseReportPage },
      {title: 'Region Wise Report', component: RegionWiseReportPage},
      {title: 'Month Wise Report', component: MonthWiseReportPage},
      {title: 'Query Sheet Report', component: MISReportPage },
      {title: 'Employee Attendence', component: EmployeeAttendancePage},
      {title: 'Client Credentials', component: ClientCredentialsPage},
      {title: 'Coordinator Credentials', component: CoordinatorCredentialsPage },
      {title: 'Employee Credentials', component: EmployeeCredentialsPage},
      {title: 'Email Settings', component: EmailSettingsPage},
      {title: 'Logout', component: LoginPage} 
    ];
    //Banalp Employee Menu
    this.BEpages = [
      { title: 'Home', component: HomePage },
      {title: 'Client Info', component: CreateClientPage},
      {title: 'Create Client', component: CreateClientPage },
      {title: 'View Client', component: CreateClientPage},
      {title: 'Coordinator Info', component: CreateCoordinatorPage},
      {title: 'Create Coordinator', component: CreateCoordinatorPage },
      {title: 'View Coordinator', component: CreateCoordinatorPage},
      {title: 'Enquiry panel', component: CreateEnquiryPage},
      {title: 'Create Enquiry', component: CreateEnquiryPage },
      {title: 'View Enquiry', component: CreateEnquiryPage},
      {title: 'Reservation', component: GeneratedReservationPage},
      {title: 'View Generated Reservation', component: GeneratedReservationPage},
      {title: 'Payment', component: PaymentHistoryPage },
      {title: 'Make Payment', component: PaymentHistoryPage},
      {title: 'Payment History', component: PaymentHistoryPage },
      {title: 'Reports', component: MISReportPage},
      {title: 'MIS Report', component: MISReportPage},
      {title: 'Client Wise Report', component: ClientWiseReportPage },
      {title: 'Region Wise Report', component: RegionWiseReportPage},
      {title: 'Month Wise Report', component: MonthWiseReportPage},
      {title: 'Query Sheet Report', component: MISReportPage },
      {title: 'Logout', component: LoginPage} 
    ];
    //Client Admin Menu
    this.CApages = [
      { title: 'Home', component: HomePage },
      {title: 'Coordinator Info', component: CreateCoordinatorPage},
      {title: 'Create Coordinator', component: CreateCoordinatorPage },
      {title: 'View Coordinator', component: CreateCoordinatorPage},
      {title: 'Enquiry panel', component: CreateEnquiryPage},
      {title: 'View Enquiry', component: CreateEnquiryPage},
      {title: 'Reservation', component: GeneratedReservationPage},
      {title: 'View Generated Reservation', component: GeneratedReservationPage},
      {title: 'Reports', component: MISReportPage},
      {title: 'Logout', component: LoginPage} 
    ];
    //Client Employee Menu
    this.CEpages = [
      {title: 'Home', component: HomePage },
      {title: 'Coordinator Info', component: CreateCoordinatorPage},
      {title: 'Create Coordinator', component: CreateCoordinatorPage },
      {title: 'View Coordinator', component: CreateCoordinatorPage},
      {title: 'Reservation', component: GeneratedReservationPage},
      {title: 'View Generated Reservation', component: GeneratedReservationPage},
      {title: 'Reports', component: MISReportPage},
      {title: 'MIS Report', component: MISReportPage},
      {title: 'Client Wise Report', component: ClientWiseReportPage },
      {title: 'Region Wise Report', component: RegionWiseReportPage},
      {title: 'Month Wise Report', component: MonthWiseReportPage},
      {title: 'Query Sheet Report', component: MISReportPage },
      {title: 'Logout', component: LoginPage} 
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page,index) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if(index === 0){
      this.nav.setRoot(TabsPage);
    }else{

      this.nav.setRoot(page.component);
    } 
  }
}
