import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ChangepasswordPage } from '../pages/changepassword/changepassword';
import { ForgetpasswordPage } from '../pages/forgetpassword/forgetpassword';
import { LoginPage } from '../pages/login/login';

import { EmployeeViewPage } from '../pages/employeeview/employeeview';
import { CreateClientPage } from '../pages/createclient/createclient';
import { CreateCoordinatorPage } from '../pages/createcoordinator/createcoordinator';
import { CreateEnquiryPage } from '../pages/createenquiry/createenquiry';
import { GeneratedReservationPage } from '../pages/reservation/generatedreservation';
import { ReservationDetailPage } from '../pages/reservationdetail/reservationdetail';
import { PaymentHistoryPage } from '../pages/paymenthistory/paymenthistory';
import { PaymentHistoryDetailPage } from '../pages/paymenthistorydetail/paymenthistorydetail';

import { MISReportPage } from '../pages/misreport/misreport';
import { MISReportDetailPage } from '../pages/misreportdetail/misreportdetail';
import { ClientWiseReportPage } from '../pages/clientwisereport/clientwisereport';
import { ClientWiseReportDetailPage } from '../pages/clientwisereportdetail/clientwisereportdetail'
import { RegionWiseReportPage } from '../pages/regionwisereport/regionwisereport';
import { MonthWiseReportDetailPage } from '../pages/monthwisereportdetail/monthwisereportdetail';
import { RegionWiseReportDetailPage } from '../pages/regionwisereportdetail/regionwisereportdetail';
import { MonthWiseReportPage } from '../pages/monthwisereport/monthwisereport';

import { EmployeeAttendancePage } from '../pages/employeeattendance/employeeattendance';
import { ClientCredentialsPage } from '../pages/clientcredentials/clientcredentials';
import { CoordinatorCredentialsPage } from '../pages/coordinatorcredentials/coordinatorcredentials';
import { EmployeeCredentialsPage } from '../pages/employeecredentials/employeecredentials';
import { EmailSettingsPage } from '../pages/emailsettings/emailsettings';

import { EmployeeDetailPage } from '../pages/employeeviewdetail/employeeviewdetail';
import { ClientDetailPage } from '../pages/clientviewdetail/clientviewdetail';
import { CoordinatorDetailPage } from '../pages/coordinatorviewdetail/coordinatorviewdetail';
import { EnquiryDetailPage } from '../pages/enquiryviewdetail/enquiryviewdetail';

import { EmployeeCreatePage } from '../pages/employeecreate/employeecreate';


import { TabsPage } from '../pages/tabs/tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import { NoshowPageModule } from '../pages/noshow/noshow.module';

@NgModule({
  declarations: [
    MyApp,
  
    HomePage,
    ChangepasswordPage,
    ForgetpasswordPage,
    LoginPage,
    TabsPage,
    EmployeeViewPage,
    EmployeeCreatePage,
    CreateClientPage,
    CreateCoordinatorPage,
    CreateEnquiryPage,
    GeneratedReservationPage,
    ReservationDetailPage,
    PaymentHistoryPage,
    PaymentHistoryDetailPage, 
    MISReportPage,
    MISReportDetailPage,
    ClientWiseReportPage,
    ClientWiseReportDetailPage,
    MonthWiseReportPage,
    MonthWiseReportDetailPage,
    RegionWiseReportPage,
    RegionWiseReportDetailPage, 
    EmployeeAttendancePage,
    ClientCredentialsPage,
    CoordinatorCredentialsPage,
    EmployeeCredentialsPage,
    EmailSettingsPage,
    EmployeeDetailPage,
    ClientDetailPage,
    CoordinatorDetailPage,
    EnquiryDetailPage,
   
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ChangepasswordPage,
    ForgetpasswordPage,
    LoginPage,
    TabsPage,
    EmployeeViewPage,
    EmployeeCreatePage,
    CreateClientPage,
    CreateCoordinatorPage,
    CreateEnquiryPage,
    GeneratedReservationPage,
    ReservationDetailPage,
    PaymentHistoryPage,
    PaymentHistoryDetailPage, 
    MISReportPage,
    MISReportDetailPage,
    ClientWiseReportPage,
    ClientWiseReportDetailPage,
    MonthWiseReportPage,
    MonthWiseReportDetailPage,
    RegionWiseReportPage,
    RegionWiseReportDetailPage, 
    EmployeeAttendancePage,
    ClientCredentialsPage,
    CoordinatorCredentialsPage,
    EmployeeCredentialsPage,
    EmailSettingsPage,
    EmployeeDetailPage,
    ClientDetailPage,
    CoordinatorDetailPage,
    EnquiryDetailPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}


